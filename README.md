From the supplied json file.

Build a Single Page Application without using a bootstrapping framework. 

1. Lists a summarised view of all the properties
2. Add filtering functionality on at least two fields in the summary view
3. When a property is clicked in the summarised view you show a detailed view of the property.
4. Ideally you'll use a frontend framework like Vue/React

Bonus points for 
- Accessibility
- Use of flexbox or grid positioning
- Use of animation
- Use of es6 syntax